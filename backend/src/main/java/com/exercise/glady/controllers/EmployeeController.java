package com.exercise.glady.controllers;

import com.exercise.glady.beans.EmployeeBean;
import com.exercise.glady.entities.Employee;
import com.exercise.glady.enums.DepositTypeEnum;
import com.exercise.glady.exceptions.EmployeeNotFoundRequestException;
import com.exercise.glady.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employee/{id}")
    @Secured("ROLE_USER")
    public ResponseEntity<Employee> getEmployee(@PathVariable("id") Long id){
        Optional<Employee> employee = employeeService.getById(id);
        if(employee.isPresent()){
            return new ResponseEntity<>(employee.get(), HttpStatus.OK);
        } else {
            throw new EmployeeNotFoundRequestException(id);
        }
    }

    @GetMapping("/employees")
    @Secured("ROLE_USER")
    public ResponseEntity<List<Employee>> getAll(){
        return new ResponseEntity<>(employeeService.getAll(), HttpStatus.OK);
    }

    @PostMapping(path = "/employee",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured("ROLE_ADMIN")
    public ResponseEntity<Employee> addCompany(@RequestBody EmployeeBean employeeBean){
        return new ResponseEntity<>(employeeService.add(employeeBean), HttpStatus.CREATED);
    }

    @PutMapping(path = "/employee/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured("ROLE_ADMIN")
    public ResponseEntity<Employee> updateEmployee(@RequestBody Employee employee){
        return new ResponseEntity<>(employeeService.update(employee), HttpStatus.OK);
    }

    @GetMapping("/employee/{id}/{type}-balance")
    @Secured("ROLE_USER")
    public ResponseEntity<BigDecimal> getDepositBalanceForEmployee(@PathVariable("id") Long id, @PathVariable("type") DepositTypeEnum depositTypeEnum){
        return new ResponseEntity<>(employeeService.estimateBalanceForEmployee(id, depositTypeEnum), HttpStatus.OK);
    }

}
