package com.exercise.glady.controllers;

import com.exercise.glady.beans.CompanyBean;
import com.exercise.glady.beans.DepositBean;
import com.exercise.glady.entities.Company;
import com.exercise.glady.entities.Deposit;
import com.exercise.glady.enums.DepositTypeEnum;
import com.exercise.glady.exceptions.CompanyNotFoundRequestException;
import com.exercise.glady.services.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @GetMapping("/company/{id}")
    @Secured("ROLE_USER")
    public ResponseEntity<Company> getCompany(@PathVariable("id") long id){
        Optional<Company> company = companyService.getById(id);
        if(company.isPresent()){
            return new ResponseEntity<>(company.get(), HttpStatus.OK);
        } else {
            throw new CompanyNotFoundRequestException(id);
        }
    }

    @GetMapping("/companies")
    @Secured("ROLE_USER")
    public ResponseEntity<List<Company>> getAll(){
        return new ResponseEntity<>(companyService.getAll(), HttpStatus.OK);
    }

    @PostMapping(path = "/company",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured("ROLE_ADMIN")
    public ResponseEntity<Company> addCompany(@RequestBody CompanyBean inputCompany){
        return new ResponseEntity<>(companyService.add(inputCompany), HttpStatus.CREATED);
    }

    @PutMapping(path = "/company/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured("ROLE_ADMIN")
    public ResponseEntity<Company> updateCompany(@RequestBody Company company){
        return new ResponseEntity<>(companyService.update(company), HttpStatus.OK);
    }

    @PostMapping(path = "/company/distribute-deposit/{type}/to-employee",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured("ROLE_ADMIN")
    public ResponseEntity<Deposit> distributeDepositToEmployee(@PathVariable("type") DepositTypeEnum type, @RequestBody DepositBean depositBean) {
        return new ResponseEntity<>(companyService.distributeDepositToEmployee(type,depositBean), HttpStatus.CREATED);
    }

}
