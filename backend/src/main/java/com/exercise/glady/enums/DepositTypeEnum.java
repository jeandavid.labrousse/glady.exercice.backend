package com.exercise.glady.enums;

public enum DepositTypeEnum {
    MEAL("meal"),
    GIFT("gift");

    final String value;

    DepositTypeEnum(String value){ this.value = value; }
}
