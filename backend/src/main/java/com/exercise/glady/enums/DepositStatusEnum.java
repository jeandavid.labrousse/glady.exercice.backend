package com.exercise.glady.enums;

public enum DepositStatusEnum {
    VALID(1),
    EXPIRED(-1),
    USED(0);

    final Integer value;

    DepositStatusEnum(Integer value){
        this.value=value;
    }
}
