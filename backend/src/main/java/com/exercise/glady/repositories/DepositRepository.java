package com.exercise.glady.repositories;

import com.exercise.glady.entities.Deposit;
import com.exercise.glady.enums.DepositStatusEnum;
import com.exercise.glady.enums.DepositTypeEnum;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.math.BigDecimal;
import java.util.List;

public interface DepositRepository<T extends Deposit> extends CrudRepository<T, Long> {

    @Query("SELECT deposit FROM Deposit deposit WHERE deposit.status = ?1")
    List<T> getAllByStatus(DepositStatusEnum status);

    @Query("SELECT deposit.amount FROM Deposit deposit WHERE deposit.status = ?1 and deposit.employee.id = ?2 and deposit.depositTypeEnum = ?3")
    List<BigDecimal> getAllAmountByStatusAndEmployeeId(DepositStatusEnum status, Long employeeId, DepositTypeEnum depositTypeEnum);

}
