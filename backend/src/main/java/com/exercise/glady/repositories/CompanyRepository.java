package com.exercise.glady.repositories;

import com.exercise.glady.entities.Company;
import org.springframework.data.repository.CrudRepository;

public interface CompanyRepository extends CrudRepository<Company, Long> {
}
