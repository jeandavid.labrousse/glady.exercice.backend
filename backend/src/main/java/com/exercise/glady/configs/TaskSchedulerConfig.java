package com.exercise.glady.configs;

import com.exercise.glady.services.DepositManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@Configuration
@EnableScheduling
public class TaskSchedulerConfig {

    @Autowired
    private DepositManagerService depositManagerService;


    @Bean
    public ThreadPoolTaskScheduler taskScheduler(){
        ThreadPoolTaskScheduler taskScheduler
                = new ThreadPoolTaskScheduler();
        taskScheduler.setPoolSize(4);
        return taskScheduler;
    }

    @Scheduled(cron="${scheduledTasks.cron.expression.checkValidityDate}")
    public void checkValiditydate(){
        depositManagerService.updateStatus();
    }
}
