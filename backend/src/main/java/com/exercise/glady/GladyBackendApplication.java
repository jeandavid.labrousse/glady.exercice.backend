package com.exercise.glady;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.exercise.glady")
@EntityScan("com.exercise.glady.entities")
public class GladyBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(GladyBackendApplication.class, args);
    }

}
