package com.exercise.glady.builders;

import com.exercise.glady.entities.Company;
import com.exercise.glady.entities.Employee;

public class EmployeeBuilder {
    private long id;
    private String firstname;
    private String lastname;
    private String mail;
    private Company company;

    public EmployeeBuilder setId(long id) {
        this.id = id;
        return this;
    }

    public EmployeeBuilder setFirstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public EmployeeBuilder setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public EmployeeBuilder setMail(String mail) {
        this.mail = mail;
        return this;
    }

    public EmployeeBuilder setCompany(Company company) {
        this.company = company;
        return this;
    }

    public Employee createEmployee() {
        return new Employee(id, firstname, lastname, mail, company);
    }
}