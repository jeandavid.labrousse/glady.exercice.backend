package com.exercise.glady.builders;

import com.exercise.glady.entities.Company;
import com.exercise.glady.entities.Employee;

import java.math.BigDecimal;
import java.util.List;

public class CompanyBuilder {
    private long id;
    private String name;

    private BigDecimal balance;

    public CompanyBuilder setId(long id) {
        this.id = id;
        return this;
    }

    public CompanyBuilder setName(String name) {
        this.name = name;
        return this;
    }


    public CompanyBuilder setBalance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    public Company createCompany() {
        return new Company(id, name, balance);
    }
}