package com.exercise.glady.utils;

import com.exercise.glady.enums.DepositTypeEnum;
import org.springframework.core.convert.converter.Converter;

public class StringToEnumConverter implements Converter<String, DepositTypeEnum> {
    @Override
    public DepositTypeEnum convert(String source) {
        return DepositTypeEnum.valueOf(source.toUpperCase());
    }
}
