package com.exercise.glady.exceptions;


public abstract class ApiRequestException extends RuntimeException{

    protected ApiRequestException(String message){
        super(message);
    }

}
