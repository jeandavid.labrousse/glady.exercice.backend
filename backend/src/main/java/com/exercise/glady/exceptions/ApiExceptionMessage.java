package com.exercise.glady.exceptions;

import org.springframework.http.HttpStatus;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ApiExceptionMessage {
    private final String message;
    private final HttpStatus status;
    private final ZonedDateTime timestamp;

    public ApiExceptionMessage(String message, HttpStatus status){
        this.message=message;
        this.status=status;
        this.timestamp= ZonedDateTime.now(ZoneId.of("Z"));
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }
}
