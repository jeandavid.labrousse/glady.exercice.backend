package com.exercise.glady.exceptions;

public class EmployeeNotInCompanyRequestException extends ApiRequestException {
    public EmployeeNotInCompanyRequestException(Long id) {
        super("The employee with id:" + id + "does not appear in Company Employee list");
    }
}
