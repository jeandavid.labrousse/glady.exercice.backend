package com.exercise.glady.exceptions;

import java.math.BigDecimal;

public class IncoherentBalanceRequestException extends ApiRequestException {
    public IncoherentBalanceRequestException(BigDecimal balance) {
        super("The balance must contains a positive value, estimated balance is:" + balance);
    }
}
