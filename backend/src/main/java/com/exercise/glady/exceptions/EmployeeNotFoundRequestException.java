package com.exercise.glady.exceptions;

public class EmployeeNotFoundRequestException extends ApiRequestException {
    public EmployeeNotFoundRequestException(Long id) {
        super("Could not find employee with id:" + id);
    }
}