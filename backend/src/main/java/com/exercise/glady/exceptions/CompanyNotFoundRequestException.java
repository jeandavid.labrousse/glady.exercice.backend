package com.exercise.glady.exceptions;

public class CompanyNotFoundRequestException extends ApiRequestException {
    public CompanyNotFoundRequestException(Long id){
        super("Could not find company with id:" + id);
    }
}
