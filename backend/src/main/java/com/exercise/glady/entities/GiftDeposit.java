package com.exercise.glady.entities;

import com.exercise.glady.beans.DepositBean;
import com.exercise.glady.enums.DepositTypeEnum;
import jakarta.persistence.Entity;

import java.time.LocalDate;

@Entity
public class GiftDeposit extends Deposit {

    public GiftDeposit(){}

    public GiftDeposit(DepositBean depositBean) {
        super(depositBean);
        determineValidityDate();
        setDepositTypeEnum(DepositTypeEnum.GIFT);
    }

    @Override
    public void determineValidityDate(){
        LocalDate validityDate = getDistributionDate().plusYears(1);
        setValidityDate(validityDate);
    }

}
