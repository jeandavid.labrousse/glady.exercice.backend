package com.exercise.glady.entities;

import com.exercise.glady.beans.DepositBean;
import com.exercise.glady.builders.EmployeeBuilder;
import com.exercise.glady.enums.DepositStatusEnum;
import com.exercise.glady.enums.DepositTypeEnum;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Deposit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private BigDecimal amount;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name="employee_id", nullable=false)
    private Employee employee;

    private LocalDate validityDate;

    private DepositStatusEnum status;

    private LocalDate distributionDate;

    private DepositTypeEnum depositTypeEnum;

    protected Deposit() {}

    protected Deposit(DepositBean depositBean) {
        this.amount = depositBean.getAmount();
        this.distributionDate = depositBean.getDistributionDate();
        this.status= DepositStatusEnum.VALID;
        EmployeeBuilder employeeBuilder = new EmployeeBuilder();
        employeeBuilder.setId(depositBean.getEmployeeId());
        this.employee = employeeBuilder.createEmployee();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public DepositStatusEnum getStatus() {
        return status;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public void setStatus(DepositStatusEnum status) {
        this.status = status;
    }

    public LocalDate getDistributionDate() {
        return distributionDate;
    }

    public void setDistributionDate(LocalDate distributionDate) {
        this.distributionDate = distributionDate;
    }


    public LocalDate getValidityDate() {
        return validityDate;
    }

    public void setValidityDate(LocalDate validityDate) {
        this.validityDate = validityDate;
    }

    public DepositTypeEnum getDepositTypeEnum() {
        return depositTypeEnum;
    }

    public void setDepositTypeEnum(DepositTypeEnum depositTypeEnum) {
        this.depositTypeEnum = depositTypeEnum;
    }

    public void determineValidityDate(){
        this.validityDate = distributionDate.plusYears(1);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Deposit deposit = (Deposit) o;
        return getId() == deposit.getId() && getAmount().equals(deposit.getAmount()) && getEmployee().getId() == deposit.getEmployee().getId() && getValidityDate().equals(deposit.getValidityDate()) && getStatus() == deposit.getStatus() && getDistributionDate().equals(deposit.getDistributionDate()) && getDepositTypeEnum() == deposit.getDepositTypeEnum();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAmount(), getEmployee(), getValidityDate(), getStatus(), getDistributionDate(), getDepositTypeEnum());
    }
}
