package com.exercise.glady.entities;

import com.exercise.glady.beans.DepositBean;
import com.exercise.glady.enums.DepositTypeEnum;
import jakarta.persistence.Entity;

import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;


@Entity
public class MealDeposit extends Deposit{


    public MealDeposit(){}

    public MealDeposit(DepositBean depositBean) {
        super(depositBean);
        determineValidityDate();
        setDepositTypeEnum(DepositTypeEnum.MEAL);
    }

    @Override
    public void determineValidityDate(){
        YearMonth month = YearMonth.from(getDistributionDate().withMonth(Month.FEBRUARY.getValue()));
        LocalDate validityDate = month.atEndOfMonth().plusYears(1);
        setValidityDate(validityDate);
    }
}
