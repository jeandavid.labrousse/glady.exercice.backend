package com.exercise.glady.entities;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

import java.util.List;
import java.util.Objects;

@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String firstname;

    private String lastname;

    private String mail;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name="company_id", nullable=false)
    private Company company;

    @OneToMany( targetEntity= MealDeposit.class, mappedBy="employee" )
    @JsonManagedReference
    private List<MealDeposit> mealDeposits;

    @OneToMany( targetEntity= GiftDeposit.class, mappedBy="employee" )
    @JsonManagedReference
    private List<GiftDeposit> giftDeposits;


    public Employee() {

    }

    public Employee(long id, String firstname, String lastname, String mail, Company company) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.mail = mail;
        this.company = company;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<MealDeposit> getMealDeposits() {
        return mealDeposits;
    }

    public void setMealDeposits(List<MealDeposit> mealDeposits) {
        this.mealDeposits = mealDeposits;
    }

    public List<GiftDeposit> getGiftDeposits() {
        return giftDeposits;
    }

    public void setGiftDeposits(List<GiftDeposit> giftDeposits) {
        this.giftDeposits = giftDeposits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return getId() == employee.getId() && getFirstname().equals(employee.getFirstname()) && getLastname().equals(employee.getLastname()) && getMail().equals(employee.getMail()) && getCompany().getId() == employee.getCompany().getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirstname(), getLastname(), getMail(), getCompany());
    }
}
