package com.exercise.glady.services;

import com.exercise.glady.beans.EmployeeBean;
import com.exercise.glady.builders.CompanyBuilder;
import com.exercise.glady.builders.EmployeeBuilder;
import com.exercise.glady.entities.Employee;
import com.exercise.glady.entities.GiftDeposit;
import com.exercise.glady.entities.MealDeposit;
import com.exercise.glady.enums.DepositTypeEnum;
import com.exercise.glady.exceptions.CompanyNotFoundRequestException;
import com.exercise.glady.repositories.EmployeeRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private CompanyService companyService;


    @Autowired
    private DepositManagerService depositManagerService;

    public Optional<Employee> getById(Long id){
        return employeeRepository.findById(id);
    }

    public List<Employee> getAll(){
        List<Employee> employees = new ArrayList<>();
        employeeRepository.findAll().forEach(employees::add);
        return employees;
    }

    @Transactional
    public Employee add(EmployeeBean employeeBean){
        if(!companyService.exists(employeeBean.getCompanyId())){
            throw new CompanyNotFoundRequestException(employeeBean.getCompanyId());
        }
        CompanyBuilder companyBuilder = new CompanyBuilder();
        companyBuilder.setId(employeeBean.getCompanyId());

        EmployeeBuilder employeeBuilder = new EmployeeBuilder();
        employeeBuilder.setFirstname(employeeBean.getFirstname());
        employeeBuilder.setLastname(employeeBean.getLastname());
        employeeBuilder.setMail(employeeBean.getMail());
        employeeBuilder.setCompany(companyBuilder.createCompany());
        return employeeRepository.save(employeeBuilder.createEmployee());
    }

    @Transactional
    public Employee update(Employee employee){
        return employeeRepository.save(employee);
    }


    public BigDecimal estimateBalanceForEmployee(Long employeeId, DepositTypeEnum depositTypeEnum){
        List<BigDecimal> depositsAmounts = depositManagerService.getDepositService(depositTypeEnum)
                .getAllDepositAmountsByEmployeeIdAndDepositType(employeeId, depositTypeEnum);
        return depositsAmounts.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }



}
