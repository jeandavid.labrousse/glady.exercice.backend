package com.exercise.glady.services;

import com.exercise.glady.entities.Deposit;
import com.exercise.glady.enums.DepositStatusEnum;
import com.exercise.glady.enums.DepositTypeEnum;
import com.exercise.glady.repositories.DepositRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class DepositService<T extends Deposit> {

    @Autowired
    private final DepositRepository<T> depositRepository;

    public DepositService(@Autowired DepositRepository<T> depositRepository){
        this.depositRepository =depositRepository;
    }

    public Optional<T> getById(Long id){
        return depositRepository.findById(id);
    }

    public List<T> getAll(){
        List<T> deposits = new ArrayList<>();
        depositRepository.findAll().forEach(deposits::add);
        return deposits;
    }

    public List<T> getAllByStatus(DepositStatusEnum status){
        return depositRepository.getAllByStatus(status);
    }

    public List<BigDecimal> getAllDepositAmountsByEmployeeIdAndDepositType(Long employeeId, DepositTypeEnum depositTypeEnum){
        return depositRepository.getAllAmountByStatusAndEmployeeId(DepositStatusEnum.VALID,employeeId, depositTypeEnum);
    }

    @Transactional
    public T addOrUpdate(T deposit){
        return depositRepository.save(deposit);
    }

    @Transactional
    public void updateStatus(){
        List<T> deposits = getAllByStatus(DepositStatusEnum.VALID);
        List<T> depositsToUpdate = new ArrayList<>();
        deposits.forEach(deposit -> {
            if(deposit.getValidityDate().isBefore(LocalDate.now())){
                deposit.setStatus(DepositStatusEnum.EXPIRED);
                depositsToUpdate.add(deposit);
            }
        });
        depositRepository.saveAll(depositsToUpdate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DepositService<?> that = (DepositService<?>) o;
        return depositRepository.equals(that.depositRepository);
    }

    @Override
    public int hashCode() {
        return Objects.hash(depositRepository);
    }
}
