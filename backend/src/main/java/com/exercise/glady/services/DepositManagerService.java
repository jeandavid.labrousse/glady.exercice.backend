package com.exercise.glady.services;

import com.exercise.glady.beans.DepositBean;
import com.exercise.glady.entities.Deposit;
import com.exercise.glady.entities.GiftDeposit;
import com.exercise.glady.entities.MealDeposit;
import com.exercise.glady.enums.DepositTypeEnum;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class DepositManagerService {

    @Autowired
    private final DepositService<MealDeposit> mealDepositService;

    @Autowired
    private final DepositService<GiftDeposit> giftDepositService;

    public DepositManagerService(@Autowired DepositService<MealDeposit> mealDepositService,@Autowired DepositService<GiftDeposit> giftDepositService ){
        this.mealDepositService=mealDepositService;
        this.giftDepositService=giftDepositService;
    }

    @Transactional
    public Deposit distributeDeposit(DepositTypeEnum depositType, DepositBean depositBean){
        switch (depositType) {
            case MEAL -> {
                MealDeposit mealDeposit = new MealDeposit(depositBean);
                return mealDepositService.addOrUpdate(mealDeposit);
            }
            case GIFT -> {
                GiftDeposit giftDeposit = new GiftDeposit(depositBean);
                return giftDepositService.addOrUpdate(giftDeposit);
            }
            default -> throw new IllegalArgumentException("This type of deposit does not exist");
        }

    }

    public DepositService<? extends Deposit> getDepositService(DepositTypeEnum depositType){
        switch (depositType) {
            case MEAL -> {
                return mealDepositService;
            }
            case GIFT -> {
                return giftDepositService;
            }
            default -> throw new IllegalArgumentException("This type of deposit does not exist");
        }
    }

    public void updateStatus(){
        mealDepositService.updateStatus();
        giftDepositService.updateStatus();
    }
}
