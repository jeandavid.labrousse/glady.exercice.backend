package com.exercise.glady.services;

import com.exercise.glady.beans.CompanyBean;
import com.exercise.glady.beans.DepositBean;
import com.exercise.glady.builders.CompanyBuilder;
import com.exercise.glady.entities.Company;
import com.exercise.glady.entities.Deposit;
import com.exercise.glady.enums.DepositTypeEnum;
import com.exercise.glady.enums.OperatorEnum;
import com.exercise.glady.exceptions.CompanyNotFoundRequestException;
import com.exercise.glady.exceptions.EmployeeNotInCompanyRequestException;
import com.exercise.glady.exceptions.IncoherentBalanceRequestException;
import com.exercise.glady.repositories.CompanyRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    DepositManagerService depositManagerService;

    public Optional<Company> getById(Long id){
        return companyRepository.findById(id);
    }

    public List<Company> getAll(){
        List<Company> companies = new ArrayList<>();
        companyRepository.findAll().forEach(companies::add);
        return companies;
    }

    @Transactional
    public Company add(CompanyBean companyBean){
        CompanyBuilder companyBuilder = new CompanyBuilder();
        companyBuilder.setName(companyBean.getName());
        companyBuilder.setBalance(companyBean.getBalance());
        return companyRepository.save(companyBuilder.createCompany());
    }

    @Transactional
    public Company update(Company company){
        return companyRepository.save(company);
    }

    public boolean exists(Long id){
        return companyRepository.existsById(id);
    }


    @Transactional
    public void modifyCompanyBalance(Long companyId, BigDecimal amount, OperatorEnum operatorEnum) {
        Company company = getById(companyId).orElseThrow(() -> new CompanyNotFoundRequestException(companyId));
        BigDecimal newBalance = company.getBalance();

        if(operatorEnum == OperatorEnum.ADD){
            newBalance = company.getBalance().add(amount);
        } else if (operatorEnum == OperatorEnum.SUB) {
            newBalance = company.getBalance().subtract(amount);
        }

        if(newBalance.compareTo(BigDecimal.ZERO)<0){
            throw new IncoherentBalanceRequestException(newBalance);
        }
        company.setBalance(newBalance);
        companyRepository.save(company);
    }

    @Transactional
    public Deposit distributeDepositToEmployee(DepositTypeEnum depositType, DepositBean depositBean){
        if(!isEmployeeInCompany(depositBean.getCompanyId(), depositBean.getEmployeeId())){
            throw new EmployeeNotInCompanyRequestException(depositBean.getEmployeeId());
        }
        modifyCompanyBalance(depositBean.getCompanyId(),depositBean.getAmount(), OperatorEnum.SUB);
        return depositManagerService.distributeDeposit(depositType,depositBean);
    }

    private boolean isEmployeeInCompany(Long companyId, Long employeeId){
        Company company = getById(companyId).orElseThrow(() -> new CompanyNotFoundRequestException(companyId));
        return company.getEmployees().stream().anyMatch(employee -> employee.getId() == employeeId);
    }

}
