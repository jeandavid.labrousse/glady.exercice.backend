package com.exercise.glady.beans;

import java.math.BigDecimal;
import java.time.LocalDate;

public class DepositBean {
    private BigDecimal amount;
    private Long employeeId;

    private Long companyId;
    private LocalDate distributionDate;

    public DepositBean(BigDecimal amount, Long employeeId, Long companyId, LocalDate distributionDate) {
        this.amount = amount;
        this.employeeId = employeeId;
        this.companyId = companyId;
        this.distributionDate = distributionDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public LocalDate getDistributionDate() {
        return distributionDate;
    }

    public void setDistributionDate(LocalDate distributionDate) {
        this.distributionDate = distributionDate;
    }


}
