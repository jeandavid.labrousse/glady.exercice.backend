package com.exercise.glady.services;

import com.exercise.glady.beans.CompanyBean;
import com.exercise.glady.builders.CompanyBuilder;
import com.exercise.glady.entities.Company;
import com.exercise.glady.enums.OperatorEnum;
import com.exercise.glady.repositories.CompanyRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class CompanyServiceTest {

    @InjectMocks
    private CompanyService companyService;

    @Mock
    private CompanyRepository companyRepository;


    private AutoCloseable autoCloseable;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }



    @Test
    void canAddCompany() {
        // given
        CompanyBean companybean = new CompanyBean();
        companybean.setName("Tesla");
        companybean.setBalance(new BigDecimal(1000));

        CompanyBuilder companyBuilder = new CompanyBuilder();
        companyBuilder.setName(companybean.getName());
        companyBuilder.setBalance(companybean.getBalance());

        Company company = companyBuilder.createCompany();
        company.setId(0L);

        // when
        companyService.add(companybean);

        // then
        ArgumentCaptor<Company> companyArgumentCaptor = ArgumentCaptor.forClass(Company.class);
        verify(companyRepository).save(companyArgumentCaptor.capture());
        Company capturedCompany = companyArgumentCaptor.getValue();

        assertThat(capturedCompany).isEqualTo(company);
    }

    @Test
    void canModifyCompanyBalance() {
        // given
        Company company = new Company(0L,"Tesla",new BigDecimal(1000));
        given(companyRepository.findById(0L)).willReturn(Optional.of(company));

        // when
        companyService.modifyCompanyBalance(0L,new BigDecimal(100),OperatorEnum.SUB);

        //then
        ArgumentCaptor<Company> companyArgumentCaptor = ArgumentCaptor.forClass(Company.class);
        verify(companyRepository).save(companyArgumentCaptor.capture());
        Company capturedCompany = companyArgumentCaptor.getValue();

        assertThat(capturedCompany.getBalance()).isEqualTo(new BigDecimal(900));

    }


}