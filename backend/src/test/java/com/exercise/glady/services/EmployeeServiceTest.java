package com.exercise.glady.services;

import com.exercise.glady.beans.EmployeeBean;
import com.exercise.glady.builders.CompanyBuilder;
import com.exercise.glady.builders.EmployeeBuilder;
import com.exercise.glady.entities.Employee;
import com.exercise.glady.entities.GiftDeposit;
import com.exercise.glady.entities.MealDeposit;
import com.exercise.glady.enums.DepositTypeEnum;
import com.exercise.glady.repositories.EmployeeRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
class EmployeeServiceTest {

    @InjectMocks
    private EmployeeService employeeService;

    @Mock
    private EmployeeRepository employeeRepository;

    @Mock
    private CompanyService companyService;

    @Spy
    @InjectMocks
    private DepositManagerService depositManagerService;

    @Mock
    private DepositService<MealDeposit> mealDepositDepositService;

    @Mock
    private DepositService<GiftDeposit> giftDepositDepositService;

    private AutoCloseable autoCloseable;


    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }


    @Test
    void canAddEmployee() {
        EmployeeBean employeeBean = new EmployeeBean();
        employeeBean.setFirstname("Elon");
        employeeBean.setLastname("Musk");
        employeeBean.setMail("elon.musk@tesla.com");
        employeeBean.setCompanyId(0L);

        CompanyBuilder companyBuilder = new CompanyBuilder();
        companyBuilder.setId(employeeBean.getCompanyId());

        EmployeeBuilder employeeBuilder = new EmployeeBuilder();
        employeeBuilder.setFirstname(employeeBean.getFirstname());
        employeeBuilder.setLastname(employeeBean.getLastname());
        employeeBuilder.setMail(employeeBean.getMail());
        employeeBuilder.setCompany(companyBuilder.createCompany());

        Employee employee = employeeBuilder.createEmployee();

        given(companyService.exists(0L)).willReturn(true);

        // when
        employeeService.add(employeeBean);

        // then
        ArgumentCaptor<Employee> employeeArgumentCaptor = ArgumentCaptor.forClass(Employee.class);
        verify(employeeRepository).save(employeeArgumentCaptor.capture());
        Employee capturedEmployee = employeeArgumentCaptor.getValue();

        assertThat(capturedEmployee).isEqualTo(employee);
    }

    @Test
    void canEstimateBalanceForEmployee() {
        // given
        BigDecimal amount1 = new BigDecimal(150);
        BigDecimal amount2 = new BigDecimal(100);
        BigDecimal amount3 = new BigDecimal(50);
        BigDecimal amount4 = new BigDecimal(75);

        List<BigDecimal> mealBalanceAmounts = Arrays.asList(amount1,amount2);
        List<BigDecimal> giftBalanceAmounts = Arrays.asList(amount3,amount4);

        BigDecimal mealBalanceSource = amount1.add(amount2);
        BigDecimal giftBalanceSource = amount3.add(amount4);

        given(depositManagerService.getDepositService(DepositTypeEnum.MEAL)
                .getAllDepositAmountsByEmployeeIdAndDepositType(0L, DepositTypeEnum.MEAL))
                .willReturn(mealBalanceAmounts);

        given(depositManagerService.getDepositService(DepositTypeEnum.GIFT)
                .getAllDepositAmountsByEmployeeIdAndDepositType(0L, DepositTypeEnum.GIFT))
                .willReturn(giftBalanceAmounts);

        // when
        BigDecimal mealBalance = employeeService.estimateBalanceForEmployee(0L, DepositTypeEnum.MEAL);
        BigDecimal giftBalance = employeeService.estimateBalanceForEmployee(0L, DepositTypeEnum.GIFT);

        // then

        assertThat(mealBalance).isEqualTo(mealBalanceSource);
        assertThat(giftBalance).isEqualTo(giftBalanceSource);

    }

}