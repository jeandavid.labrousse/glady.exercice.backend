package com.exercise.glady.services;

import com.exercise.glady.beans.DepositBean;
import com.exercise.glady.entities.GiftDeposit;
import com.exercise.glady.entities.MealDeposit;
import com.exercise.glady.enums.DepositStatusEnum;
import com.exercise.glady.repositories.DepositRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class GiftDepositServiceTest {

    @Spy
    @InjectMocks
    private DepositService<GiftDeposit> giftDepositService;
    @Mock
    private DepositRepository<GiftDeposit> giftDepositRepository;

    @Captor
    private ArgumentCaptor<List<GiftDeposit>> giftDepositListCaptor;


    @Test
    void canAddOrUpdateGiftDeposit(){
        // given
        DepositBean depositBean = new DepositBean(new BigDecimal(150),0L,0L, LocalDate.now());
        GiftDeposit giftDeposit = new GiftDeposit(depositBean);

        // when
        giftDepositService.addOrUpdate(giftDeposit);

        // then
        ArgumentCaptor<GiftDeposit> giftDepositArgumentCaptor = ArgumentCaptor.forClass(GiftDeposit.class);
        verify(giftDepositRepository).save(giftDepositArgumentCaptor.capture());
        GiftDeposit capturedGiftDeposit = giftDepositArgumentCaptor.getValue();

        assertThat(capturedGiftDeposit).isEqualTo(giftDeposit);

    }


    @Test
    void canUpdateStatusForGiftDeposit(){
        // given
        LocalDate distributionDate = LocalDate.now().minusYears(1).minusDays(1);
        DepositBean depositBeanToExpire = new DepositBean(new BigDecimal(50),0L,0L, distributionDate);
        DepositBean depositBean = new DepositBean(new BigDecimal(75),0L,0L, LocalDate.now());
        GiftDeposit giftDepositToExpire = new GiftDeposit(depositBeanToExpire);
        GiftDeposit giftDeposit = new GiftDeposit(depositBean);

        List<GiftDeposit> giftDepositList = Arrays.asList(giftDepositToExpire,giftDeposit);

        given(giftDepositRepository.getAllByStatus(DepositStatusEnum.VALID)).willReturn(giftDepositList);

        // when
        giftDepositService.updateStatus();

        verify(giftDepositRepository).saveAll(giftDepositListCaptor.capture());
        List<GiftDeposit> capturedGiftDeposit = giftDepositListCaptor.getValue();

        assertThat(capturedGiftDeposit.size()).isEqualTo(1);
        assertThat(capturedGiftDeposit.get(0).getStatus()).isEqualTo(DepositStatusEnum.EXPIRED);

    }

}
