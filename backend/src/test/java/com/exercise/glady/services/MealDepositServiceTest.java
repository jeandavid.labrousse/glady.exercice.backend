package com.exercise.glady.services;

import com.exercise.glady.beans.DepositBean;
import com.exercise.glady.entities.GiftDeposit;
import com.exercise.glady.entities.MealDeposit;
import com.exercise.glady.enums.DepositStatusEnum;
import com.exercise.glady.repositories.DepositRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class MealDepositServiceTest {

    @Spy
    @InjectMocks
    private DepositService<MealDeposit> mealDepositService;

    @Mock
    private DepositRepository<MealDeposit> mealDepositRepository;

    @Captor
    private ArgumentCaptor<List<MealDeposit>> mealDepositListCaptor;



    @Test
    void canAddOrUpdateMealDeposit(){
        // given
        DepositBean depositBean = new DepositBean(new BigDecimal(100),0L,0L, LocalDate.now());
        MealDeposit mealDeposit = new MealDeposit(depositBean);

        // when
        mealDepositService.addOrUpdate(mealDeposit);

        // then
        ArgumentCaptor<MealDeposit> mealDepositArgumentCaptor = ArgumentCaptor.forClass(MealDeposit.class);
        verify(mealDepositRepository).save(mealDepositArgumentCaptor.capture());
        MealDeposit capturedMealDeposit = mealDepositArgumentCaptor.getValue();

        assertThat(capturedMealDeposit).isEqualTo(mealDeposit);

    }


    @Test
    void canUpdateStatusForMealDeposit(){
        // given
        LocalDate distributionDate = LocalDate.now().minusYears(1);
        DepositBean depositBeanToExpire = new DepositBean(new BigDecimal(150),0L,0L, distributionDate);
        DepositBean depositBean = new DepositBean(new BigDecimal(100),0L,0L, LocalDate.now());
        MealDeposit mealDepositToExpire = new MealDeposit(depositBeanToExpire);
        MealDeposit mealDeposit = new MealDeposit(depositBean);

        List<MealDeposit> mealDepositList = Arrays.asList(mealDepositToExpire,mealDeposit);

        given(mealDepositRepository.getAllByStatus(DepositStatusEnum.VALID)).willReturn(mealDepositList);

        // when
        mealDepositService.updateStatus();

        verify(mealDepositRepository).saveAll(mealDepositListCaptor.capture());
        List<MealDeposit> capturedMealDeposit = mealDepositListCaptor.getValue();

        assertThat(capturedMealDeposit.size()).isEqualTo(1);
        assertThat(capturedMealDeposit.get(0).getStatus()).isEqualTo(DepositStatusEnum.EXPIRED);
    }


}
